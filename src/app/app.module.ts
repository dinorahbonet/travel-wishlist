import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { DestinationsListComponent } from './destinations-list/destinations-list.component';

@NgModule({
  declarations: [
    AppComponent,
    DestinationsComponent,
    DestinationsListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
