export class DestinyTravel {
  name: string;
  imageUrl: string;

  constructor(n: string, u: string) {
    this.name = n;
    this.imageUrl = u;
  }
}
