import { Component, OnInit } from '@angular/core';
import { DestinyTravel } from '../models/destiny-travel.model';

@Component({
  selector: 'app-destinations-list',
  templateUrl: './destinations-list.component.html',
  styleUrls: ['./destinations-list.component.css'],
})
export class DestinationsListComponent implements OnInit {
  destinies: DestinyTravel[];

  constructor() {
    this.destinies = [];
  }

  ngOnInit(): void {}

  save(name: string, url: string): boolean {
    this.destinies.push(new DestinyTravel(name, url));
    console.log(this.destinies);
    return false;
  }
}
