import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { DestinyTravel } from '../models/destiny-travel.model';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.css'],
})
export class DestinationsComponent implements OnInit {
  @Input()
  destiny!: DestinyTravel;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() {
    // this.destiny=[];
  }

  ngOnInit(): void {}
}
